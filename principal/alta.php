<?php
include '../includes/header.php';
?>
<?php
include '../includes/sesion.php';
?>

<div class="container">
    <div class="row" style="width:50%;margin-top:50px;">
            <div class="col s12 m12 l12 justify-content-center">
            <div class="card white ">
                <div class="card-content black-text">
                <span class="card-title">Nuevo Snippet</span>
                    <form id="altaSnippet" autocomplete="off" @submit.prevent="alta">
                        <div class="row">
                            <div class="input-field col s12">
                            <input id="titulo" type="text" class="validate" name="titulo" required>
                            <label for="titulo">Titulo</label>
                            <span class="helper-text" data-error="Completa el campo" data-success="Completado!"></span>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col s12">
                            <textarea id="codigo" class="materialize-textarea validate" name="codigo" required></textarea>
                            <label for="codigo">Escribe tu codigo</label>
                            <span class="helper-text" data-error="Completa el campo" data-success="Completado!"></span>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col s12">
                            <textarea id="descripcion" class="materialize-textarea validate" name="descripcion" required></textarea>
                            <label for="descripcion">Escribe tu descripcion</label>
                            <span class="helper-text" data-error="Completa el campo" data-success="Completado!"></span>
                            </div>
                        </div>
                        <div class="input-field col s12">
                            <select class="browser-default" name="categoria">
                            <option value="" disabled selected>Selecciona una opcion</option>
                            <option value="html">HTML</option>
                            <option value="php">PHP</option>
                            <option value="java">JAVA</option>
                            <option value="vue">VUE</option>
                            <option value="css">CSS</option>
                            </select>
                            
                        </div>
                        <button class="btn waves-effect green" type="submit">Postear
                            <i class="material-icons right">send</i>
                        </button>


                    </form>
                </div>
                <div class="card-action">   
                        <h5>{{resp}}</h5>    
                </div>
            </div>
        </div>
    </div>
</div>

<?php
include '../includes/footer.php';
?>