<?php
include '../includes/header.php';
?>
<?php
include '../includes/sesion.php';
?>

    <nav>
        <div class="nav-wrapper #00e676 green accent-3 center">
        <form>
            <div class="input-field">
            <input id="search" type="search" v-model="buscar" required>
            <label class="label-icon" for="search"><i class="material-icons">search</i></label>
            <i class="material-icons">close</i>
            </div>
        </form>
        </div>
    </nav>
<div class="container">
    <div class="row" v-for="item in datosFiltrados">
            <div class="col s12 m12 l12 justify-content-center">
            <div class="card white ">
                <div class="card-content black-text">
                <span class="card-title"><img :src="item.imagen" :alt="item.imagen" width="50px " class="circle"> {{item.usuario}}</span>
                <span class="card-title">{{item.titulo}}</span>
                    <pre :id="'copy' + item.id">
                        {{item.codigo}} {{userPost}}
                    </pre>
                    <p>{{item.descripcion}}</p>
                    <label>#{{item.categoria}}</label>
                    

                </div>

                <div class="card-action">
                    <a v-if="item.usuario == userPost" :href="'/php-vuejs.gi/principal/editar.php?id=' + item.id">Editar</a> 
                    <a v-if="item.usuario == userPost" href="#" @click="eliminar(item.id)">Eliminar</a> 
                    <a href="#" class="copiar" :data-clipboard-target="'#copy' + item.id">Copiar</a> 
                             
                </div>
            </div>
        </div>
    </div>
</div>
<?php
include '../includes/footer.php';
?>