<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <!--Materialize-->
    <!-- Compiled and minified CSS -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">
    <!-- Compiled and minified JavaScript -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>
    <!--ICONS-->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    
    <title>Snippets</title>
</head>
<body>
    <main id="app">

    <nav class="green">
        <div class="nav-wrapper" v-if="menu == true">
            <a href="#" class="brand-logo">Snippets</a>
            <ul id="nav-mobile" class="right hide-on-med-and-down">
                <li><a href="index.php" disable><i class="material-icons right">home</i>Inicio</a></li>
                <li><a href="alta.php"><i class="material-icons right">add</i>Agregar Snippet</a></li>
                <li><a href="../login/salir.php"><i class="material-icons right">power_settings_new</i>Cerrar sesion</a></li>
            </ul>
        </div>
    </nav>



    
    
