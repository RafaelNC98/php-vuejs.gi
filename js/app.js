const app = new Vue({
    el:'#app',
    data:{
        menu: true,
        resp: '',
        listar:[],
        buscar:'',
        itemId:'',
        formEditar: {},
        userPost:''
    },
    created(){
        axios.get('http://localhost:8082/php-vuejs.gi/api/crud/getPost.php')
        .then(res => {
            this.listar = res.data
        })
        this.getId()
        this.getUser()


    },
    computed:{
        datosFiltrados(){
            return this.listar.filter((filtro)=>{
                return filtro.titulo.toUpperCase().match(this.buscar.toUpperCase()) || filtro.descripcion.toUpperCase().match(this.buscar.toUpperCase()) ||
                filtro.usuario.toUpperCase().match(this.buscar.toUpperCase()) || filtro.categoria.match(this.buscar)
            })
        }

    },
    methods:{
        alta(){
            const form = document.getElementById('altaSnippet')
            axios.post('../api/crud/altaPost.php', new FormData(form))
            .then( res=>{
                this.resp = res.data
                if(res.data == 'success'){
                    swal({
                        title:'Correcto!',
                        text:'El Snippet se registro correctamente',
                        icon:'success'
                        
                    })
                    form.reset();
                    
                    
                }else{
                    swal({
                        title:'Oops...',
                        text:'No se pudo registrar el Snippet',
                        icon:'error'
                    })
                }
            })
        },
        getId(){
            let uri = window.location.href.split('?');
            if(uri.length == 2){
                let vars = uri[1].split('&');
                let getVars = {};
                let tmp = '';
                vars.forEach(function(v){
                    tmp = v.split('=');
                    if(tmp.length == 2){
                        getVars[tmp[0]] = tmp[1];
                    }
                });
                this.itemId = getVars
                console.log(this.itemId.id)
                axios.get('http://localhost:8082/php-vuejs.gi/api/crud/getId.php?id='+ this.itemId.id)
                .then(res =>{
                    this.formEditar = res.data
                })
            }


        },
        editar(){
            const form = document.getElementById('editarPost')
            axios.post('../api/crud/editarPost.php', new FormData(form))
            .then( res=>{
                this.resp = res.data
                if(res.data == 'success'){                    
                    location.href = 'index.php'
                }else{
                    swal({                        
                        title:'Oops...',
                        text:'No se pudo editar el Snippet',
                        icon:'error'
                    })
                }
            })
        },
        eliminar(id){
            swal({
                title:'Deseas eliminar el post?',
                text:'Una vez eliminado, no podras recuperarlo',
                icon: 'warning',
                buttons: true,
                dangerMode: true
            })
            .then((aceptar)=>{
                if(aceptar){                    
                    axios.get('http://localhost:8082/php-vuejs.gi/api/crud/eliminar.php?id=' + id)
                    .then(res=>{
                        this.resp = res.data
                        if(res.data == 'success'){
                            swal('Listo! Tu post ha sido eliminado',{
                                icon:"success"
                            })
                        }else{
                            swal('Estuvo cerca!, No sucedio nada con tu post',{
                                icon:'error'
                            }) 
                        }
                    })                    
                }else{
                   return false;
                }
            })
        },
        getUser(){
            axios.get('http://localhost:8082/php-vuejs.gi/api/crud/getUser.php')
            .then(res =>{
                this.userPost = res.data
            })
        }
    }
})