const app = new Vue({
    el:'#app',
    data:{
        pass:'',
        passC:'',
        resp:'',
        email:'',
        passwd:'',
        menu: false
    },
    methods:{
        registro(){
            if (this.pass == this.passC) {
                const form = document.getElementById('formRegistro')
                axios.post('../api/loginRegistro/registro.php', new FormData(form))
                .then( res=>{
                    this.resp = res.data
                    this.direccionamiento()
                })
            } else {
                alert('Error! las contraseñas no coinciden')
            }
        },
        direccionamiento(){
            if (this.resp == 'success') {
                location.href = 'index.php'
            } else {
                swal({
                    title: 'Error al registrar',
                    text: 'Verifique su informacion',
                    icon: 'error'
                })
            }
        },
        login(){
            const form = document.getElementById('inicioSesion')
            axios.post('../api/loginRegistro/login.php', new FormData(form))
            .then( res=>{
                if (res.data == 'success') {
                    location.href = '../principal'
                } else {
                    swal({
                        title: 'Error al iniciar sesion',
                        text: 'Verifique su informacion',
                        icon: 'error'
                    })
                }
 
            })
        }
    }
})