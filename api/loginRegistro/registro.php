<?php
include '../conexion.php';

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $usuario = htmlentities($_POST['usuario']); 
    $contrasena = htmlentities($_POST['contrasena']);
    $correo = htmlentities($_POST['correo']);

    $extension = '';
    $ruta = '../api/loginRegistro/imagen_perfil';
    $archivo = $_FILES['imagen']['tmp_name'];
    $nombreArchivo = $_FILES['imagen']['name'];
    $info = pathinfo($nombreArchivo);
    if ($archivo != '') {
        $extension = $info['extension'];

        if ($extension == 'png' || $extension == 'PNG' || $extension == 'jpg' || $extension == 'JPG') {
            $nomFile = $usuario.rand(0000,9999).'.'.$extension;
            move_uploaded_file($archivo,'imagen_perfil/'.$nomFile);
            $ruta = $ruta.'/'.$nomFile;
        } else {
           echo "fail";
           exit;
        }
        


    } else {
        $ruta = '../api/loginRegistro/imagen_perfil/perfil.png';
    }
    
    $passEncrip = password_hash($contrasena,PASSWORD_BCRYPT);
    $ins = $con->query("INSERT INTO usuarios(usuario,contrasena,correo,imagen) VALUES('$usuario','$passEncrip','$correo','$ruta');");
    if ($ins) {
        echo 'success';
    } else {
        echo 'fail';
    }
    

} else {
    header("Location:../../index.php");
}



?>