<?php
include '../conexion.php';

if($_SERVER['REQUEST_METHOD'] == 'POST'){
    $titulo = htmlentities($_POST['titulo']);
    $codigo = htmlentities($_POST['codigo']);
    $descripcion = htmlentities($_POST['descripcion']);
    $categoria = htmlentities($_POST['categoria']);

    $id = null;
    $usuario = $_SESSION['usuario'];
    $imagen = $_SESSION['imagen'];

    $ins = $con->prepare("INSERT INTO snippet VALUES(?,?,?,?,?,?,?);");
    $ins->bind_param("issssss",$id,$usuario,$imagen,$titulo,$codigo,$descripcion,$categoria);

    if($ins->execute()){
        echo "success";
    }else{
        echo "fail";
    }

    $ins->close();
    $con->close();
}


?>