<?php @session_start();
if(isset($_SESSION['usuario'])){
    header("location:../principal");
}
include '../includes/header.php';
?>

<div class="container">
    <div class="row" style="width:50%;margin-top:50px;">
            <div class="col s12 m12 l12 justify-content-center">
            <div class="card white ">
                <div class="card-content black-text">
                <span class="card-title">Inicio de sesion</span>
                    <form id="inicioSesion" autocomplete="off" @submit.prevent="login">
                        <div class="row">
                            <div class="input-field col s12">
                            <input id="email" type="email" class="validate" name="email" required>
                            <label for="email">Correo Electrónico</label>
                            <span class="helper-text" data-error="Correo Electrónico no valido" data-success="Completado!" required>ejemplo@ejemplo.com</span>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col s12">
                            <input id="password" type="password" name="passwd" required class="validate" pattern="[A-Za-z0-9]{8,16}">
                            <label for="password">Contraseña</label>
                            <span class="helper-text" data-error="Contraseña no valida" data-success="Completado!">8-16 Caracteres, Numeros y Letras</span>
                            </div>
                        </div>
                        <button class="btn waves-effect green" type="submit">Entrar
                            <i class="material-icons right">send</i>
                        </button>


                    </form>
                </div>
                <div class="card-action">
                <a href="registro.php" class="btn waves-effect blue white-text">Registrarse</a>
                
                </div>
            </div>
        </div>
    </div>
</div>

<?php
include '../includes/footerLogin.php';
?>