<?php
include '../includes/header.php';
?>
    <div class="container">
        <div class="row" style="width:50%;margin-top:50px;">
            <div class="col s12 m12 l12 justify-content-center">
            <div class="card white ">
                <div class="card-content black-text">
                    <span class="card-title" style="text-align:center;">Registrarse</span>
                        <form id="formRegistro" @submit.prevent="registro" enctype="multipart/form-data">
                            <div class="row">
                                <div class="input-field col s12">
                                <input id="first_name2" type="text" name="usuario" class="validate" required pattern="[A-Zz-a]{5,10}">
                                <label class="active" for="first_name2">Nombre de Usuario</label><span class="helper-text" data-error="Nombre de Usuario no valido" data-success="Completado!">5-10 Caracteres,Mayusculas y Minusculas</span>
                                </div>
                            </div>
                            <div class="row">
                                <div class="input-field col s12">
                                <input id="password" type="password" name="contrasena" v-model="pass" class="validate" pattern="[A-Za-z0-9]{8,16}" required>
                                <label for="password">Contraseña</label>
                                <span class="helper-text" data-error="Contraseña no valida" data-success="Completado!">8-16 Caracteres, Numeros y Letras</span>
                                </div>
                            </div>
                            <div class="row">
                                <div class="input-field col s12">
                                <input id="password" type="password" v-model="passC" class="validate" pattern="[A-Za-z0-9]{8,16}" required>
                                <label for="password">Repetir Contraseña</label>
                                <span class="helper-text" data-error="Contraseña no valida" data-success="Completado!">8-16 Caracteres, Numeros y Letras</span>
                                </div>
                            </div>
                            <div class="row">
                                <div class="input-field col s12">
                                <input id="email" type="email" class="validate" name="correo" required>
                                <label for="email">Correo Electrónico</label>
                                <span class="helper-text" data-error="Correo Electrónico no valido" data-success="Completado!" required>ejemplo@ejemplo.com</span>
                                </div>
                            </div>
                            <div class="file-field input-field">
                            <div class="btn">
                                <span>Imagen de Perfil</span>
                                <input type="file" name="imagen">
                            </div>
                            <div class="file-path-wrapper">
                                <input class="file-path validate" type="text">
                            </div>
                            </div>
                            <button class="btn waves-effect green" type="submit">Registrarse
                                <i class="material-icons right">send</i>
                            </button>
                        </form>
                </div>
            </div>
            <div class="card-action">
            <a href="index.php" class="btn green accent-2 black-text">Iniciar sesion</a>
        </div>
    </div>
<?php
include '../includes/footerLogin.php';
?>